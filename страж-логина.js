/// Действия с пользователем
//const crypto = require('crypto');
const budden_tools = require('./budden_tools');
const хеш_и_соль = require('./хеш_и_соль');
const Бд = require('./бд').Бд;
//const jsonwebtoken = require('jsonwebtoken');

async function ОбработчикВхода (req, res, next) {
    budden_tools.ИгнорируйАргументы(next);
    console.log('Входим в обработчик входа');

    if (req.session.user) {
        console.log('Мы уже вошли - нет нужды входить снова');
        return res.redirect('/статья');
    }

    const body = req.body;
    const { username, password } = body;
    //const valid = username.length && password === '123'
    const valid = await ПроверьЛогин(username, password, Бд);
    console.log('ПроверьЛогин вернул: ', valid);
  
    switch (valid) {
      case 0: throw new Error('Пользователь не найден'); 
      case 1: throw new Error('Неверный пароль'); 
      case 2: {
          req.session.user = { user_name: username };
          res.redirect('/все-статьи');
          break;
      }
      default: throw new Error('Внутренняя ошибка');
    }

    //res.json( req.session.user );
  }



  async function ОбработчикВыхода (req, res, next) {
    budden_tools.ИгнорируйАргументы(next);

    if (req.session.user) {
        delete req.session.user;
		res.redirect('/');
    }
  }


  // отправляет почту. А ведь можно заддосить нас, отправляя заявки на пользователей.  
async function НачниДобавлениеПользователя(псевдоним, пароль, электропочта) {
    const Соед = await Бд.connect();
    try {
        const хеш = await хеш_и_соль.hashPassword(пароль);
        const Д = { хеш, псевдоним, электропочта };
        // Не факт, что это хорошо - не можем получить список запросов.
        const Рез = await Соед.result("insert into пользователь (код, подтверждён, ${this:name}) "
            +" values"
            +" (nextval('следующий_код_пользователя'), 0, ${this:csv})",Д);
        if (Рез.rowCount !== 1) {
            throw new Error('Ошибка при вставке заявки на добавление пользователя'); 
        }
    } catch (и) {
        console.error(и);
        return и;
    } finally {
        Соед.done();
    }
}

// возващает 0 - нет пользователя, 1 - неверный пароль, 2 - ок. 
async function ПроверьЛогин(Псевдоним, Пароль, Бд) {
    console.log('входим в ПроверьЛогин');
    const Соед = await Бд.connect();
    try {
        const Д = await Соед.oneOrNone('select код,хеш,подтверждён from пользователь where псевдоним=$1 limit 1',[Псевдоним]);
        console.log('База вернула пользователя: ',Д);
        if (Д === null) {
            return 0;
        } else {
            // https://stackoverflow.com/questions/17201450/salt-and-hash-password-in-nodejs-w-crypto
           const Подходит = await хеш_и_соль.verifyPassword( Пароль, Д.хеш );
            if (Подходит) {
               return 2;
            } else {
               return 1; 
            }
        }
    } catch (и) {
        console.error(и);
        return 3;
    } finally {
        Соед.done();
    }
}

// для теста:
//НачниДобавлениеПользователя('budden','123','budden73@mail.ru');

module.exports = {
    ПроверьЛогин
    ,ОбработчикВхода, ОбработчикВыхода
    ,НачниДобавлениеПользователя
};

