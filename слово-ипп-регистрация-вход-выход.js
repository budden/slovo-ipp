/// Точка сбора защищённой серверной части - здесь назначаются маршруты
const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const budden_tools = require('./budden_tools.js');

const страж_логина = require('./страж-логина');

const асинхронныйОбработчикЭкспресса = require('express-async-handler');

// Install middleware
router.use(bodyParser.json());

// -- Routes --

router.post(encodeURI('/заявляю-нового-пользователя'), 
    асинхронныйОбработчикЭкспресса(страж_логина.НачниДобавлениеПользователя));

// [POST] /login77
router.post('/login', асинхронныйОбработчикЭкспресса(страж_логина.ОбработчикВхода));
router.post('/logout', асинхронныйОбработчикЭкспресса(страж_логина.ОбработчикВыхода));

// [GET] /user
router.get('/user', (req, res, next) => {
  budden_tools.ИгнорируйАргументы(next);
  if (req.session !== undefined && req.session.user !== undefined) {
    res.json({ user: req.session.user });
  } else {
    res.status(401).send("Вы не вошли");
  }
});

// Error handler
router.use((err, req, res, next) => {
  budden_tools.ИгнорируйАргументы(next);
  console.error(err); // eslint-disable-line no-console
  res.status(401).send(err + '');
});

// И некоторые маршруты находятся в основном файле слово-ипп-главная.js

// -- export router --
module.exports = {
  path: '/api/auth',
  handler: router
};
