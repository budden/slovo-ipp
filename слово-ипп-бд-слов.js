var budden_tools = require('./budden_tools');
const Бд = require('./бд').Бд;

async function СохраниСтатью(Запр, Рез, След) {
  budden_tools.ИгнорируйАргументы(След);
  const Соед = await Бд.connect();
  try {
    const Д = {код: 5, слово: "form", перевод: "<к>form</к> Форма"};
    const РезВставки = await Соед.any('insert into английское_слово (код, слово, перевод) values (${код},${слово},${перевод})'
      ,Д);
    Рез.status(200).json(РезВставки); // не факт, что он сюда вообще полезет
  } catch (и) {
    Рез.status(500).send("Ошибка записи в БД");
  } finally {
    Соед.done();
  }
}

async function ОтправьСтатью(Запр, Рез, След) {
    budden_tools.ИгнорируйАргументы(След);
    const Соед = await Бд.connect();
    try {
        const Слова = await Соед.oneOrNone('select код,слово,перевод from английское_слово limit 1');
        //Слова2 = { код: 7, слово: "House", перевод: "Дом"};
        Рез.status(200).json(Слова);
    } catch (и) {
        Рез.status(500).send("Ошибка доступа к БД");
        console.error(и);
    } finally {
        Соед.done();
    }
}

async function ОтправьВсеСтатьи(Запр, Рез, След) {
    budden_tools.ИгнорируйАргументы(След);
    const Соед = await Бд.connect();
    try {
        const Слова = await Соед.any('select код,слово,перевод from английское_слово');
        Рез.status(200).json(Слова);
    } catch (и) {
        Рез.status(500).send("Ошибка доступа к БД");
        console.error(и);
    } finally {
        Соед.done();
    }
}


module.exports = {
    СохраниСтатью, ОтправьВсеСтатьи, ОтправьСтатью
}
