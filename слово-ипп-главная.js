var express = require('./express-with-https');
var multer = require('multer'); // v1.0.5
var upload = multer(); // for parsing multipart/form-data
const бд_слов = require('./слово-ипп-бд-слов');
const cookieParser = require('cookie-parser');
const session = require('express-session');

var app = express();

var MemoryStore = require('memorystore')(session);
// Install middleware
app.use(cookieParser());
app.use(session({
  secret: 'ПРАВЬМЯ 324'
  ,resave: false
  ,saveUninitialized: true
  ,cookie: { secure: true, maxAge: 86400000 }
  ,name: "slovo"
  ,unset: 'destroy'
  ,store: new MemoryStore({
    checkPeriod: 3600000 
  })  
}));


app.use('/static', express.static('static'));

const lodash_express = require('lodash-express'); 
lodash_express(app,'html');
app.set('views', './lodash');
app.set('view engine','html');

const api_auth = require('./слово-ипп-регистрация-вход-выход');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT");
    next(); 
  });

// eslint-disable-next-line  no-unused-vars
var isProduction = process.env.NODE_ENV === 'production';

const асинхронныйОбработчикЭкспресса = require('express-async-handler');

app.get('/', function (req, res) {
    res.render('index', { field1: 'blub', title: 'Hey', message: 'Hello there!'});
  });

app.get(encodeURI('/вход'), function (req,res) { 
    let Параметры = { ужеВошлиКак : undefined };
    if (req.session !== undefined && req.session.user !== undefined) {
        Параметры = { ужеВошлиКак : req.session.user.user_name };
    };
    res.render('форма-логина', Параметры);
}); 

app.get(encodeURI('/все-статьи'), асинхронныйОбработчикЭкспресса(бд_слов.ОтправьВсеСтатьи));
app.get(encodeURI('/статья'), асинхронныйОбработчикЭкспресса(бд_слов.ОтправьСтатью));
api_auth.handler.put(encodeURI('/сохрани-статью'), асинхронныйОбработчикЭкспресса(бд_слов.СохраниСтатью));

app.use(upload.array());

app.use(api_auth.path,api_auth.handler);

const httpsServer = express.создайHttpsСервер(app);

httpsServer.listen(8445, function() {
    console.log('Сервер запущен: https://localhost:8445/');

    if (process.send) {
        process.send('online');
    }
});
