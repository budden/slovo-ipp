function настройКириллицуДляШаблоновИменованныхПараметров__pgPromise () {
    var patterns = require('pg-promise/lib/patterns');
    var np = patterns.namedParameters;
    patterns.namedParameters = /\$(?:({)|(\()|(<)|(\[)|(\/))\s*[a-zA-Z0-9а-яА-ЯёЁ$_.]+(\^|~|#|:raw|:alias|:name|:json|:csv|:list|:value)?\s*(?:(?=\2)(?=\3)(?=\4)(?=\5)}|(?=\1)(?=\3)(?=\4)(?=\5)\)|(?=\1)(?=\2)(?=\4)(?=\5)>|(?=\1)(?=\2)(?=\3)(?=\5)]|(?=\1)(?=\2)(?=\3)(?=\4)\/)/g;
  }
  
  // надо как-то гарантировать, что это происходит до загрузки pg-promise. А как? 
настройКириллицуДляШаблоновИменованныхПараметров__pgPromise();
  
const pgp = require('pg-promise')();

const фс = require('fs');
const утил = require('util');

const Пароль = фс.readFileSync(__dirname + '/пароль.текст')

const СтрокаСоединения = утил.format('postgres://den:%s@localhost/slovo',Пароль);

const Бд = pgp(СтрокаСоединения);

module.exports = { Бд };
