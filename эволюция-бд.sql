CREATE TABLE public."пользователь"
(
  "код" bigint,
  "электропочта" character varying(256),
  "псевдоним" character varying(256),
  "подтверждён" integer,
  "хеш" character varying(256)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."пользователь"
  OWNER TO den;

create unique index индекс_пользователь_псевдоним on пользователь (псевдоним);
