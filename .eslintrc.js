module.exports = {
    "env": {
        "browser": false,
        "commonjs": true,
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module",
        "ecmaVersion": 2017
    },
    "rules": {
        "no-console":0,
        "indent": [
            0,
            2
        ],
        "linebreak-style": [
            1,
            "unix"
        ],
        "semi": [
            1,
            "always"
        ]
    }
};