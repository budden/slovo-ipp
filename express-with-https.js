const fs = require('fs');
const http = require('http');
const https = require('https');
const privateKey  = fs.readFileSync(__dirname+'/sslcert/server.key', 'utf8');
const certificate = fs.readFileSync(__dirname+'/sslcert/server.crt', 'utf8');

const credentials = {key: privateKey, cert: certificate};
const express = require('express');

// your express configuration here

function создайHttpsСервер(app) {
    return https.createServer(credentials, app);
} 

express.создайHttpsСервер = создайHttpsСервер;

module.exports = express;

